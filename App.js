import 'react-native-gesture-handler';
import React from 'react';
import AppNavigation from './src/navigations/AppNavigation';

const App = () => <AppNavigation/>

export default App;