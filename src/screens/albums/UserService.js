import { APP_TOKEN, REQUEST_HEADERS } from "../../constants";

const getUsers = async () => {
    const resp = await fetch('https://gorest.co.in/public-api/users');
    return await resp.json();
}

const createUser = async (data) => {
    const resp = await fetch('https://gorest.co.in/public-api/users', {
        method: 'POST',
        headers: REQUEST_HEADERS,
        body: JSON.stringify(data),
    });

    return await resp.json();
}

const editUser = async (data) => {
    const resp = await fetch(`https://gorest.co.in/public-api/users/${data.id}`, {
        method: 'PATCH',
        headers: REQUEST_HEADERS,
        body: JSON.stringify(data),
    });

    return await resp.json();
}

const deleteUser = async (id) => {
    const resp = await fetch(`https://gorest.co.in/public-api/users/${id}`, {
        method: 'DELETE',
        headers: REQUEST_HEADERS,
    });

    return await resp.json();
}

export { getUsers, createUser, editUser, deleteUser };