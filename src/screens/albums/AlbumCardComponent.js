import React from 'react';
import { StyleSheet, TouchableOpacity, Text, Alert } from "react-native";
import Icon from 'react-native-vector-icons/Feather';


const AlbumCardCompoent = ({ data, onEditClicked, onDeleteClicked }) => {

    const handleDelete = () => {
        Alert.alert(
            "Delete User",
            "Are you sure to remove this user ?",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => onDeleteClicked(data.id) }
            ]
          );
    }

    return (
        <TouchableOpacity style={styles.container} onPress={() => onEditClicked(data)}>
            <Icon
                name='x'
                size={24}
                onPress={handleDelete}
                style={styles.title}
                />
            <Text>{data.name}</Text>
            <Text>{data.status}</Text>
        </TouchableOpacity>
    );
}

export default AlbumCardCompoent;

const styles = StyleSheet.create({
    container: {
        height: 124,
        backgroundColor: 'white',
        display: 'flex',
        borderRadius: 8,
        marginBottom: 10,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        position: 'absolute',
        top: 10,
        right: 10
    },
})